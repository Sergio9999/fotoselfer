FactoryGirl.define do
  factory :user do
    name                  'Sergio999'
    sequence(:email)      { |i| "email#{i}@email.ru" }
    password              '111222'
    password_confirmation '111222'
  end
end
