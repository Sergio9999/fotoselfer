require 'rails_helper'

feature 'Users' do
  let(:submit) { 'Create my account' }
  let(:user) { FactoryGirl.create(:user) }
  subject { page }
  describe 'Profile page' do
    before { visit user_path(user) }
    it { expect have_selector('h1', text: 'User Profile') }
    it { expect have_selector('h4', text: user.name) }
    it { expect have_selector('h4', text: user.email) }
    it { expect have_selector('img') }
  end

  describe 'with invalid information' do
    before { visit new_user_path }
    it { expect have_content('Avatar') }
    it 'should not create user' do
      expect { click_button submit }.not_to change(User, :count)
    end

    describe 'after click on submit' do
      before { click_button submit }
      it { expect have_content(text: 'errors') }
      it { expect have_content(text: 'Registration') }
    end
  end

  describe 'with valid information' do
    before { visit new_user_path }
    before do
      fill_in 'Name', with: user.name
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      fill_in 'Confirmation', with: user.password_confirmation
    end
    it 'should be create user' do
      click_button submit
      expect { page }.to change(User, :count).by(1)
    end

    describe 'after saving user' do
      before { click_button submit }
      it { expect have_title(user.name) }
      it { expect has_link?('Sign out') }
      it { expect have_selector('div.alert.alert-info') }
      it { expect have_content(user.name) }
    end
  end
end
