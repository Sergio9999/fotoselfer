require 'rails_helper'

feature 'Update User' do
  subject { page }
  let(:user) { FactoryGirl.create(:user) }
  describe 'with invalid information' do
    before { visit user_path(user) }
    it { expect have_content('errors') }
    it { expect have_selector('div.alert.alert-alert') }
    it { expect have_selector('h1', text: 'Update profile') }
  end

  describe 'when you click to link Update' do
    before { visit edit_user_path(user) }
    it { expect have_content('name') }
    it { expect have_content('email', 'password', 'password_confirmation') }
    it { expect have_link('Home') }
    it { expect have_selector('h1', 'Update profile') }
  end

  describe 'with valid information' do
    before { visit edit_user_path(user) }
    it 'should be update user' do
      fill_in 'name', with: user.name
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      fill_in 'Confirmation', with: user.password_confirmation

      click button submit
    end
    it { expect have_content('Profile updated') }
    it { expect have_content(user.name) }
    it { expect have_content(user.email) }
    it { expect have_selector('h1', 'User profile') }
    it { expect have_content('Name') }
    it { expect have_content('Email') }
  end
end
