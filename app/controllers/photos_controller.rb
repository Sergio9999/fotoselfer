class PhotosController < ApplicationController
  before_action :signin_albums

  def create
    params[:photos]['image'].each do |image|
      @photo = @album.photos.create!(image: image)
    end
    if @photo.save
      flash[:success] = 'Your images was upload'
    else
      flash[:error] = 'Error occurred while uploading photos'
    end
    redirect_to @album
  end

  def destroy
    @photo = @album.photos.find(params[:id])
    @photo.destroy
  end

  def signin_albums
    @album = Album.find(params[:album_id])
  end
end
