class OmniauthController < ApplicationController
  def create
    @account = Account.find_by(uid: auth_hash[:uid],
                               provider: auth_hash[:provider])
    if @account
    else
      @user = User.find_by(email: auth_hash[:email])
      if @user
        @account = Account.create(uid: auth_hash[:uid],
                                  provider: auth_hash[:provider],
                                  email: auth_hash[:info][:email],
                                  user: @user)
      else
        @account = Account.find_or_create_by(uid: auth_hash[:uid],
                                             provider: auth_hash[:provider],
                                             email: auth_hash[:info][:email])
        @user = @account.create_user(auth_params)
        @account.save
        UserMailer.inviters_mailer(@user).deliver_now
        flash[:success] = 'Letter with Our password is sending to your email. Thank you!!!'
      end
    end
    session[:user_id] = @account.user_id
    redirect_to current_user
  end

  private

  def auth_params
    password = "#{rand(100)}_sample_password_#{rand(100)}"
   { name: auth_hash[:info][:name],
     password: password,
     email: auth_hash[:info][:email],
     password_confirmation: password
   }
  end

  def auth_hash
    request.env['omniauth.auth']
  end
end
