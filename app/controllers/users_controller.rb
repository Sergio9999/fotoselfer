class UsersController < ApplicationController
  before_action :signin_user, only: [:show, :edit, :update]

  def new
    @user = User.new
  end

  def index
    @users = User.all
  end

  def create
    @user = User.new(user_params)
    if @user.save
      UserMailer.signup_confirmation(@user).deliver
      session[:user_id] = @user.id
      redirect_to @user.email
      flash[:success] = 'Thank you for signed up!!!'
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
  end

  def update
    if current_user.update_attributes(user_params)
      flash[:success] = 'Profile updated'
      redirect_to current_user
    else
      render 'edit'
    end
  end

  def destroy
    @album.destroy
    flash[:success] = 'Your album was destroy'
    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation, :avatar)
  end

  def signin_user
    @user = User.find(params[:id])
    redirect_to new_session_path unless current_user == @user
  end
end
