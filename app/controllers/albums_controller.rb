class AlbumsController < ApplicationController
  before_action :signin_albums, only: [:show, :edit, :update, :destroy]

  def index
    @albums = current_user.albums.all
  end

  def new
    @album = Album.new
    @photo = @album.photos.build
  end

  def create
    @album = current_user.albums.build(album_params)
    if @album.save
      unless photo_blank
        upload_photo
      end
      flash[:success] = 'Your album was create!'
      redirect_to @album
    else
      render 'new'
    end
  end

  def show
    @photos = @album.photos.all
  end

  def edit
  end

  def update
    if @album.update_attributes(album_params)
      unless photo_blank
        upload_photo
      end
      flash[:success] = 'Your album update'
      redirect_to @album
    else
      render 'edit'
    end
  end

  def destroy
    @album.destroy
  end

  def upload_photo
    params[:photos]['image'].each do |image|
      @photo = @album.photos.create!(image: image)
    end
  end

  def photo_blank
    params[:photos].nil?
  end

  private

  def album_params
    params.require(:album).permit(:title, :description, photos_attributes: [:album_id, :image])
  end

  def signin_albums
    @album = Album.find(params[:id])
  end
end
