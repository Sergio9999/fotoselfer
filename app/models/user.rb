class User < ActiveRecord::Base
  before_save { self.email = email.downcase }
  validates :name, presence: true, on: :create
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, on: :create
  has_many :accounts, dependent: :delete_all
  has_many :albums, dependent: :delete_all
  mount_uploader :avatar, AvatarUploader
end
