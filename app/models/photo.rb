class Photo < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  belongs_to :album
  validates :album_id, presence: true
end
