class Album < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true
  has_many :photos, dependent: :delete_all

  accepts_nested_attributes_for :photos, allow_destroy: true
end
