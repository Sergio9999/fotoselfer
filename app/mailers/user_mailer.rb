class UserMailer < ApplicationMailer
  default from: 'SobolevSergey9999@gmail.com'

  def signup_confirmation(user)
    @user = user
    mail to: @user.email, subject: 'Welcome to PhotoSelfer'
  end

  def inviters_mailer(user)
    @user = user
    mail to: @user.email, subject: 'PhotoSelfer'
  end
end
