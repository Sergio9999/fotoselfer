class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "albums_photos/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :small do
    process :resize_to_fit => [200, 200]
  end

  version :medium do
    process :resize_to_fit => [400, 400]
  end

  version :large do
    process :resize_to_fit => [800, 800]
  end
end
