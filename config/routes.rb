Rails.application.routes.draw do
  root 'static_pages#index'
  resources :users
  resources :sessions
  resources :albums do
    resources :photos, only: [:create, :destroy]
  end
  delete :signout, to: 'sessions#destroy'

  get 'auth/:provider/callback', to: 'omniauth#create'
  get '/help', to: 'static_pages#help'
end
