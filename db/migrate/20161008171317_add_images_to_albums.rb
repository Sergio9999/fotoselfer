class AddImagesToAlbums < ActiveRecord::Migration
  def change
    add_column :albums, :images, :json
  end
end
