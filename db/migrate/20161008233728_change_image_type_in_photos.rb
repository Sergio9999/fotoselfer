class ChangeImageTypeInPhotos < ActiveRecord::Migration
  def change
  	change_column :photos, :images, :string
  end
end
