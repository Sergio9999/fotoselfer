class RemovePhotosFromAlbums < ActiveRecord::Migration
  def change
    remove_column :albums, :photos, :string
  end
end
