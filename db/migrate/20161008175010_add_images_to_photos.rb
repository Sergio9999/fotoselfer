class AddImagesToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :images, :json
    change_column :photos, :images, :string
  end
end
