class RemoveColumnSlidesFromAlbums < ActiveRecord::Migration[5.0]
  def change
    remove_column :albums, :slides, :text
  end
end
