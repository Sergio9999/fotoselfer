class RemoveImagesFromAlbums < ActiveRecord::Migration
  def change
    remove_column :albums, :images, :json
  end
end
