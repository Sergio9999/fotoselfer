class AddSlidesToAlbums < ActiveRecord::Migration
  def change
    add_column :albums, :slides, :text
  end
end
